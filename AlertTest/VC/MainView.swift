//
//  ConnectedDeviceVC.swift
//  AlertTest
//
//  Created by EIC on 2018-10-12.
//  Copyright © 2018 Darby. All rights reserved.
//

import UIKit

class MainView: UIViewController {
    
    @IBOutlet weak var ConnectionStatusLabel: UILabel!
    @IBOutlet weak var CountsLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(doSomethingAfterNotifiedCT007),
                                               name: NSNotification.Name(rawValue: myCT007NotificationKey),
                                               object: nil)
    }
    
    
    // Called every second
    @objc func doSomethingAfterNotifiedCT007 (){
        //MARK: connection status .....
        ConnectionStatusLabel.text = ConnectionState
        CountsLabel.text = String(Radiation_Characteristics_Value_Int)
//        print ("noti simple : " , String(Radiation_Characteristics_Value_Int))
    }

}
