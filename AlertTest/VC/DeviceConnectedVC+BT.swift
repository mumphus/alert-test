//
//  DeviceConnectedVC+BT.swift
//  AlertTest
//
//  Created by EIC on 2018-10-17.
//  Copyright © 2018 Darby. All rights reserved.
//

import Foundation
import CoreBluetooth


extension DeviceConnectedVC: CBPeripheralDelegate {
    func centralManager(_ central: CBCentralManager, didFailToConnectPeripheral peripheral: CBPeripheral, error: NSError?) {
        print ("connected view 006")
        print("Error connecting peripheralVar: \(String(describing: error?.localizedDescription))")
    }
    func centralManager(_ central: CBCentralManager, didConnect peripheral: CBPeripheral) {
        // Wait 4 seconds incase device is still booting up
        MillisecondsTimer = 0
        showConnectingAlert()
        DispatchQueue.main.asyncAfter(deadline: .now() + 4) {
            peripheral.discoverServices(nil)
            self.readAfterConnect()
            // Restart the display refresh timer
            if !(rssiReloadTimer?.isValid)!{
                rssiReloadTimer = nil
                rssiReloadTimer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(DeviceConnectedVC.refreshRSSI), userInfo: nil, repeats: true)
                RunLoop.current.add(rssiReloadTimer!, forMode: RunLoop.Mode.common)
            }
        }
    }
    
    func peripheral(_ peripheral: CBPeripheral, didDiscoverServices error: Error?){
        print ("connected view 006-001")
        for service in peripheral.services! {
            let thisService = service as CBService
            if CT007.isoadService(thisService) {
                print ("Discovered oad Service")
                peripheral.discoverCharacteristics(nil, for: thisService)
            }
            if CT007.isCommService(thisService) {
                print ("Discovered Comm service")
                peripheral.discoverCharacteristics(nil, for: thisService)
            }
            if CT007.isBatteryService(thisService) {
                print ("Discovered battery service")
                peripheral.discoverCharacteristics(nil, for: thisService)
            }
            if CT007.isRadiationService(thisService) {
                print ("Discovered radiation service")
                peripheral.discoverCharacteristics(nil, for: thisService)
            }
        }
        if error != nil {
            print("Error discovering services: \(String(describing: error?.localizedDescription))")
        }
        services.removeAll()
        ids.removeAll()
        peripheral.services?.forEach({ (service) in
            services.append(service)
            ids.append(service.uuid)
            peripheral.discoverCharacteristics(nil, for: service)
        })
    }
    func peripheral(_ peripheral: CBPeripheral, didDiscoverCharacteristicsFor service: CBService, error: Error?) {
        for charateristic in service.characteristics! {
            let thisCharacteristic = charateristic as CBCharacteristic
            if CT007.isInputCharacteristic(thisCharacteristic) {
                print ("Input charac notify 014")
                self.peripheralVar?.setNotifyValue(true, for: thisCharacteristic)
            }
            if CT007.isOutputCharacteristic(thisCharacteristic) {
                MyOutPutChar = thisCharacteristic
                print ("Output charac write 014")
            }
            if CT007.isImgIdentityCharacteristic(thisCharacteristic) {
                print ("Img Identity charac read ,  \(thisCharacteristic)")
                self.peripheralVar?.readValue(for: thisCharacteristic)
            }
            if CT007.isimgBlockCharacteristic(thisCharacteristic) {
                print ("img block charac read  ,  \(thisCharacteristic)")
                self.peripheralVar?.readValue(for: thisCharacteristic)
            }
            if CT007.isRadiationCharacteristic(thisCharacteristic) {
                print ("radiation charac notify ,  \(thisCharacteristic)")
                self.peripheralVar?.setNotifyValue(true, for: thisCharacteristic)
            }
            if CT007.isConversionCharacteristic(thisCharacteristic) {
                print ("conversion charac read ,  \(thisCharacteristic)")
                self.peripheralVar?.readValue(for: thisCharacteristic)
            }
            if CT007.isBatteryCharacteristic(thisCharacteristic) {
                print ("battery charac read ,  \(thisCharacteristic)")
                self.peripheralVar?.readValue(for: thisCharacteristic)
            }
        }
        if error != nil {
            print("Error discovering service characteristics: \(String(describing: error?.localizedDescription))")
        }
        service.characteristics?.forEach({ (characteristic) in
            print("charact: -->  \(String(describing: characteristic.descriptors))---\(characteristic.properties)")
        })
    }
    
    
    // MARK: Get data values when they are updated
    func peripheral(_ peripheral: CBPeripheral, didUpdateValueFor characteristic: CBCharacteristic, error: Error?) {
        if characteristic.uuid == batteryLevel {
            if let BatteryCharVal = characteristic.value {
                var bytes = Array(repeating: 0 as UInt8, count:BatteryCharVal.count/MemoryLayout<UInt8>.size)
                //print (bytes)
                BatteryCharVal.copyBytes(to: &bytes, count:BatteryCharVal.count)
                let data16 = bytes.map { UInt8($0) }
                let data = Data(bytes: data16)
                BatteryLevel_Characteristics_Value = UInt32(littleEndian: data.withUnsafeBytes { $0.pointee })
                print ("BatteryLevel_Characteristics_Value:  " ,BatteryLevel_Characteristics_Value)
            }
            BatteryLevel_Characteristics_Value_Int = Int(BatteryLevel_Characteristics_Value)
        }
        
        if characteristic.uuid == Conversion_Factor {
            if let ConversionFactorCharVal = characteristic.value {
                var bytes = Array(repeating: 0 as UInt8, count:ConversionFactorCharVal.count/MemoryLayout<UInt8>.size)
                //print (bytes)
                ConversionFactorCharVal.copyBytes(to: &bytes, count:ConversionFactorCharVal.count)
                let data16 = bytes.map { UInt8($0) }
                let data = Data(bytes: data16)
                ConversionFactor_Characteristics_Value = UInt32(littleEndian: data.withUnsafeBytes { $0.pointee })
                print ("writepending - ConversionFactor_Characteristics_Value:  " ,ConversionFactor_Characteristics_Value)
            }
        }
        if characteristic.uuid == radiationCount {
            if let radiationCount = characteristic.value {
                var bytes = Array(repeating: 0 as UInt8, count:radiationCount.count/MemoryLayout<UInt8>.size)
                //print (bytes)
                radiationCount.copyBytes(to: &bytes, count:radiationCount.count)
                let data16 = bytes.map { UInt8($0) }
                let data = Data(bytes: data16)
                Radiation_Characteristics_Value = UInt32(littleEndian: data.withUnsafeBytes { $0.pointee })
            }
            Radiation_Characteristics_Value_Int += Int(Radiation_Characteristics_Value)
            print("Radiation Update: \(Radiation_Characteristics_Value)")
        }
        
        // recieves and saves data from detector
        if characteristic.uuid == inputCharacteristicUUID {
            print("THIS IS SEND MESSAGE:",SentMsg)
            if let str1 = characteristic.value {
                let str2 = NSString(data: str1, encoding: String.Encoding.ascii.rawValue)!
                let str3 = str2 as String
                var textArray: [String]  = []
                for line in str3.components(separatedBy: "\n") {
                    if !line.hasPrefix("//") {
                        textArray.append(line)
                    }
                    print ("line : ", textArray)
                }
                
                let str = textArray.joined(separator: "\n")
                for ch in str {
                    writepending.append(ch)
                    if ch == "\n" {
                        //print ("writepending init =  ", writepending)
                        DeviceRespond = writepending
                        Console_Setting_Text_Respond = DeviceRespond
                        writepending = ""
                        if DeviceRespond == "OK\n" {
                            print ("Respond AAA 001 is OK: --> ", Console_Setting_Text_Respond)
                        } else {
                            if SentMsg == command_RESPOND_TEST{
                                print ("Device Response was: ", DeviceRespond)
                                if DeviceRespond != "TEST\n"{
                                    MillisecondsTimer = 0
                                    print("ERROR RESTARTING DEVICE SETTINGS")
                                }else{
                                    print("device check passed")
                                }
                            }
                            else if SentMsg == command_PRINT_V {
                                print ("writepending = conversion factor is : ", DeviceRespond)
                                GOT_CONVERSION = DeviceRespond
                                let GOT_CONVERSION_truncated = GOT_CONVERSION[..<GOT_CONVERSION.index(before: GOT_CONVERSION.endIndex)]
                                if (Int(GOT_CONVERSION_truncated) != 0) {
                                    print ("writepending-" , Int(GOT_CONVERSION_truncated)!)
                                    if Int(GOT_CONVERSION_truncated) != nil {
                                        print ("writepending-" , Int(GOT_CONVERSION_truncated)!)
                                        ConversionFactor_Characteristics_Value_Int = Int(GOT_CONVERSION_truncated)!
                                        CT007V = ConversionFactor_Characteristics_Value_Int
                                        let dValue:Int = 166667/CT007V
                                    }
                                }
                            }
                            else if SentMsg == command_PRINT_VERSION {
                                print ("writepending = Device Basic Version is  : ", DeviceRespond)
                                GOT_BASIC_VERSION = DeviceRespond
                            }
                            else if SentMsg == command_PRINT_DEVICE_ID {
                                print ("writepending = Device ID is  : ", DeviceRespond)
                                GOT_DEVICE_ID = DeviceRespond
                            }
                            else if SentMsg == command_PRINT_Beep {
                                print ("writepending = Device beep state is  : ", DeviceRespond)
                                GOT_BEEP_STATE = DeviceRespond
                                let GOT_BEEP_STATE_truncated = GOT_BEEP_STATE[..<GOT_BEEP_STATE.index(before: GOT_BEEP_STATE.endIndex)]
                                if (Int(GOT_BEEP_STATE_truncated) == 1) {
                                    //                                    IsDetectorBeepingEnabled = true
                                }else{
                                    //                                    IsDetectorBeepingEnabled = false
                                }
                            }
                            else if SentMsg == command_PRINT_RTime {
                                print ("writepending = Device Response Time is  : ", DeviceRespond)
                                GOT_RTime = DeviceRespond
                                //Save value
                                let GOT_RTime_truncated = GOT_RTime[..<GOT_RTime.index(before: GOT_RTime.endIndex)]
                                //                                CT007ResponseTime = Int(GOT_RTime_truncated)!
                                if let responseInt = Int(GOT_RTime_truncated){
                                    //                                    CT007ResponseTime = responseInt
                                    //                                    CT007DeviceResponseCapable = true
                                }else{
                                    //                                    CT007DeviceResponseCapable = false
                                    print("Could not read response time")
                                }
                            }
                            else if SentMsg == command_PRINT_DTime {
                                print ("writepending = Device Dead Time is  : ", DeviceRespond)
                                GOT_DTime = DeviceRespond
                                //Save value
                                let GOT_DTime_truncated = GOT_DTime[..<GOT_DTime.index(before: GOT_DTime.endIndex)]
                                if let deadTimeInt = Int(GOT_DTime_truncated){
                                    //                                    CT007DeadTime = deadTimeInt
                                    //                                    CT007DeviceDeadTimeCapable = true
                                }else{
                                    //                                    CT007DeviceDeadTimeCapable = false
                                    print("Could not read dead time")
                                }
                            }
                            else if SentMsg == command_PRINT_Alarm {
                                print ("writepending = Device alarm state is  : ", DeviceRespond)
                                GOT_ALARM_STATE = DeviceRespond
                                let GOT_ALARM_STATE_truncated = GOT_ALARM_STATE[..<GOT_ALARM_STATE.index(before: GOT_ALARM_STATE.endIndex)]
                            }
                            else if SentMsg == command_PRINT_Alarm_Level {
                                print ("writepending = Device alarm level is  : ", DeviceRespond)
                                GOT_ALARM_LEVEL = DeviceRespond
                                let GOT_ALARM_LEVEL_truncated = GOT_ALARM_LEVEL[..<GOT_ALARM_LEVEL.index(before: GOT_ALARM_LEVEL.endIndex)]
                            }
                            else if SentMsg == command_PRINT_SCREENS {
                                print ("writepending = Device screens state is  : ", DeviceRespond)
                                GOT_Screens = DeviceRespond
                                let GOT_Screens_truncated = GOT_Screens[..<GOT_Screens.index(before: GOT_Screens.endIndex)]
                                var startIndex = 0
                                for i in GOT_Screens_truncated{
                                    //                                    CT007Screens[startIndex] = Int(String(i))!
                                    startIndex += 1
                                }
                            }
                            else if SentMsg == command_PRINT_UNITS {
                                print ("writepending = Device units state is  : ", DeviceRespond)
                                GOT_Units = DeviceRespond
                                let GOT_Units_truncated = GOT_Units[..<GOT_Units.index(before: GOT_Units.endIndex)]
                            }
                            else if SentMsg == command_LIST {
                                if DeviceRespond.hasPrefix("3630"){
                                    if let match = DeviceRespond.range(of:"(?<=\")[^\"]+", options: .regularExpression) {
                                        DeviceRespond = String(DeviceRespond[match])
                                        //print(DeviceRespond.substring(with: match))
                                        GOT_DEVICE_NAME = DeviceRespond
                                        //                                        DetectorNameLabel.text = "getting Detector Name--->  \(GOT_DEVICE_NAME)"
                                        Detector_NAME = GOT_DEVICE_NAME
                                        print ("writepending = Device NAME is  : ", DeviceRespond)
                                    }
                                }
                            }
                            SentMsg = ""
                        }
                    }
                }
                print ("DeviceRespond: " , DeviceRespond)
            } else {
                print ("---------- writepending --------- CANT DO IT")
            }
        }
        print ("DeviceRespond: " , DeviceRespond)
    }
    
    func peripheral(_ peripheral: CBPeripheral, didReadRSSI RSSI: NSNumber, error: Error?) {
        IntRSSI = Int(truncating: RSSI)
        print ("RSSI:  \(RSSI.intValue)")
    }
    
}

extension DeviceConnectedVC: CBCentralManagerDelegate {
    func centralManagerDidUpdateState(_ central: CBCentralManager) {
    }
    func centralManager(_ central: CBCentralManager, didFailToConnect peripheral: CBPeripheral, error: Error?) {
    }
    func centralManager(_ central: CBCentralManager, didDiscover peripheral: CBPeripheral, advertisementData: [String : Any], rssi RSSI: NSNumber) {
        if peripheral == peripheralVar{
            print("Found previous peripheral")
            Singleton.sharedInstance.centralManager?.connect(peripheralVar!, options: nil)
        }
    }
    func centralManager(_ central: CBCentralManager, didDisconnectPeripheral peripheral: CBPeripheral, error: Error?) {
        MillisecondsTimer = 0
        showDisconnectAlert()
        Singleton.sharedInstance.centralManager?.scanForPeripherals(withServices: ids, options: nil)
        if consoleTimer != nil {
            consoleTimer?.invalidate()
            consoleTimer = nil
        }
    }
    
    
}
