//
//  PeripheralTableViewController.swift
//  BLEScanner
//
//  Created by Sepehr Khatir on 15/03/2017.
//  Copyright © 2017 EIC. All rights reserved.
//

import CoreBluetooth
import UIKit
import Foundation
import SystemConfiguration
import UserNotifications

var FirstHomeLoad = true

struct DisplayPeripheral{
    var peripheral: CBPeripheral?
    var lastRSSI: NSNumber?
    var isConnectable: Bool?
}

class ViewController: UIViewController, UITableViewDelegate {
    
//    @IBOutlet weak var statusLabel: UILabel!
//    @IBOutlet weak var bluetoothIcon: UIImageView!
//    @IBOutlet weak var scanningButton: ScanButton!
    
    var RefreshScanCounter = 0
    var peripherals: [DisplayPeripheral] = []
    var viewReloadTimer: Timer?
    
    var selectedPeripheral: CBPeripheral?
    
    @IBOutlet weak var tableView: UITableView!
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        //Initialise CoreBluetooth Central Manager
        Singleton.sharedInstance.centralManager = CBCentralManager(delegate: self, queue: DispatchQueue.main)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tabBarController?.tabBar.isHidden = true
        self.extendedLayoutIncludesOpaqueBars = true
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.reloadData()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destinationViewController = segue.destination as? DeviceConnectedVC{
            destinationViewController.peripheralVar = selectedPeripheral
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationController?.navigationBar.barTintColor = UIColor.orange
        viewReloadTimer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(ViewController.refreshScanView), userInfo: nil, repeats: true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        viewReloadTimer?.invalidate()
    }
    
    
    func updateViewForScanning(){
//        if self.statusLabel != nil {
//            statusLabel.text = " Scanning BLE Devices..."
//            bluetoothIcon.isHidden = false
//            scanningButton.buttonColorScheme(true)
//        } else {
//            print ("ERROR ON updateViewForScanning --> statuslabel is nil ")
//        }
    }
    
    func updateViewForStopScanning(){
//        let plural = peripherals.count > 1 ? "s" : ""
//        statusLabel.text = " \(peripherals.count) Device\(plural) Found"
//        bluetoothIcon.layer.removeAllAnimations()
//        bluetoothIcon.isHidden = true
//        scanningButton.buttonColorScheme(false)
    }
    
    @IBAction func scanpressed(_ sender: Any) {
        if Singleton.sharedInstance.centralManager!.isScanning{
            Singleton.sharedInstance.centralManager?.stopScan()
            updateViewForStopScanning()
        }else{
            startScanning()
        }
    }
    
    func startScanning(){
        RefreshScanCounter = 1
        peripherals = []
        Singleton.sharedInstance.centralManager?.scanForPeripherals(withServices: nil, options: [CBCentralManagerScanOptionAllowDuplicatesKey: true])
        updateViewForScanning()
        let triggerTime = (Int64(NSEC_PER_SEC) * 10)
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double(triggerTime) / Double(NSEC_PER_SEC), execute: { () -> Void in
            if (Singleton.sharedInstance.centralManager!.isScanning) && (self.RefreshScanCounter >= 9) {
                Singleton.sharedInstance.centralManager?.stopScan()
                self.updateViewForStopScanning()
            }
        })
    }
    
    @objc func refreshScanView()
    {
        if peripherals.count >= 0 && Singleton.sharedInstance.centralManager!.isScanning{
            tableView.reloadData()
            if RefreshScanCounter > 12 {
                RefreshScanCounter = 0
            }
            RefreshScanCounter += 1
        }
    }
}

extension ViewController: CBCentralManagerDelegate{
    func centralManagerDidUpdateState(_ central: CBCentralManager){
        if (central.state == .poweredOn){
            startScanning()
        }else{
            // do something like alert the user that ble is not on
        }
    }
    
    func centralManager(_ central: CBCentralManager, didDiscover peripheral: CBPeripheral, advertisementData: [String : Any], rssi RSSI: NSNumber){
        
        for (index, foundPeripheral) in peripherals.enumerated(){
            if foundPeripheral.peripheral?.identifier == peripheral.identifier{
                peripherals[index].lastRSSI = RSSI
                return
            }
        }
        
        let isConnectable = advertisementData["kCBAdvDataIsConnectable"] as! Bool
        let displayPeripheral = DisplayPeripheral(peripheral: peripheral, lastRSSI: RSSI, isConnectable: isConnectable)
        peripherals.append(displayPeripheral)
        tableView.reloadData()
    }
}

extension ViewController: CBPeripheralDelegate {
    func centralManager(_ central: CBCentralManager, didFailToConnect peripheral: CBPeripheral, error: Error?) {
        print("Error connecting peripheral: \(String(describing: error?.localizedDescription))")
    }
    
    func centralManager(_ central: CBCentralManager, didConnect peripheral: CBPeripheral) {
        print ("perform segu PeripheralConnectedSegue 0000")
        performSegue(withIdentifier: "segue123", sender: self)
        peripheral.discoverServices(nil)
        Singleton.sharedInstance.centralManager?.stopScan()
    }
}

extension ViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        
        let cell = self.tableView.dequeueReusableCell(withIdentifier: "cell")! as! DeviceTableViewCell
        cell.displayPeripheral = peripherals[indexPath.row]
        cell.delegate = self
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return peripherals.count
    }
}

extension ViewController: DeviceCellDelegate{
    func connectPressed(_ peripheral: CBPeripheral) {
        if peripheral.state != .connected {
            selectedPeripheral = peripheral
            peripheral.delegate = self
            print ("selectedPeripheral...... 002 \(String(describing: selectedPeripheral))")
            Singleton.sharedInstance.centralManager?.connect(peripheral, options: nil)
        }
    }
}

