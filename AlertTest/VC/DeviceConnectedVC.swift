//
//  DeviceConnectedVC.swift
//  AlertTest
//
//  Created by Darby on 2018-10-10.
//  Copyright © 2018 Darby. All rights reserved.
//

import UIKit
import CoreBluetooth

var rssiReloadTimer: Timer?
var Radiation_Characteristics_Value_Int = 0
var periphGlob: CBPeripheral?
let myCT007NotificationKey = "CT007NotificationKey"
var Detector_NAME: String? = ""
var ConnectionState = ""
var Connected: Bool = false
var BatteryLevel_Characteristics_Value_Int = 0
var Console_Setting_Text_Respond = ""
var CT007V: Int = 0
var IntRSSI = 0

class DeviceConnectedVC: UIViewController {
    
    //-----------------------
    // Variables
    //-----------------------
    var services: [CBService] = []
    var OneSecTimer = 0
    var MillisecondsTimer = 0
    var peripheralVar: CBPeripheral?
    var CT_Device: Bool = false
    var consoleTimer: Timer?
    var MyOutPutChar:CBCharacteristic?
    var SentMsg = ""
    var ids = [CBUUID]()
    
    var disconnectAlertController : UIAlertController?
    var connectingAlertController : UIAlertController?
    let reconnectionTime = 30
    var timeToReconnect = -1
    var presentedMainView = false
    //-----------------------
    // Variables
    //-----------------------
    
    //-----------------------
    // Commands
    //-----------------------
    var command_RESPOND_TEST = "PRINT 'TEST'"
    var command_STOP_TIMER0 = "TIMER 0 STOP"
    var command_STOP_TIMER1 = "TIMER 1 STOP"
    var command_STOP_TIMER2 = "TIMER 2 STOP"
    var command_RUN_TIMER_0 = "TIMER 0, 200 REPEAT GOSUB 2100"
    var command_RUN_TIMER_1 = "TIMER 0 STOP"
    var command_RUN_TIMER_2 = "TIMER 2, 1000 REPEAT GOSUB 2110"
    var command_BEFORE_TIMERS = "T(34)=15"
    var command_LIST = "list 3630"
    var command_1 = "1"
    var command_PRINT_V = "print V"
    var command_PRINT_VERSION = "print T(31),\".\",T(32)"
    var command_PRINT_DEVICE_ID = "print T(0)"
    var command_PRINT_Beep = "print T(2)"
    var command_PRINT_Alarm = "print T(12)"
    var command_PRINT_Alarm_Level = "print F"
    var command_PRINT_RTime = "print T(37)"
    var command_PRINT_DTime = "PRINT W"
    var command_PRINT_SCREENS = "PRINT X(1),X(2),X(3),X(4)"
    var command_PRINT_UNITS = "PRINT T(36)"
    var command_test = "2800 DELAY 1"
    var command_CLEAR_COUNTER = "counter 0, clear"
    //-----------------------
    // Commands
    //-----------------------
    
    //-----------------------
    // Receiving Inputs
    //-----------------------
    var GOT_CONVERSION = ""
    var GOT_DEVICE_ID = ""
    var test = ""
    var GOT_BASIC_VERSION = ""
    var GOT_DEVICE_NAME = ""
    var GOT_BEEP_STATE = ""
    var INT_GOT_BEEP_STATE = 0
    var GOT_ALARM_STATE = ""
    var INT_GOT_ALARM_STATE = 0
    var GOT_ALARM_LEVEL = ""
    var INT_GOT_ALARM_LEVEL = 0
    var GOT_RTime = ""
    var GOT_cFactor = ""
    var INT_GOT_cFactor: Double = 0
    var GOT_DTime = ""
    var GOT_Screens = ""
    var GOT_Units = ""
    var INT_GOT_RTime = 0
    
    var writepending = ""
    var DeviceRespond = ""
    //-----------------------
    // Receiving Inputs
    //-----------------------
    
    //-----------------------
    // Characteristic Values
    //-----------------------
    var ConversionFactor_Characteristics_Value: UInt32 = 1
    var BatteryLevel_Characteristics_Value: UInt32 = 0
    var Radiation_Characteristics_Value: UInt32 = 0
    var ConversionFactor_Characteristics_Value_Int: Int = 1
    var INPUT_Characteristics_Value: UInt32 = 0
    var INPUT_Characteristics_Value_Int = 0
    //-----------------------
    // Characteristic Values
    //-----------------------

    override func viewDidLoad() {
        super.viewDidLoad()
        peripheralVar?.delegate = self
        Detector_NAME = peripheralVar?.name
        self.navigationItem.title = Detector_NAME
        Singleton.sharedInstance.centralManager?.delegate = self
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(doThisWhenNotifyCT007),
                                               name: NSNotification.Name(rawValue: myCT007NotificationKey),
                                               object: nil)
        rssiReloadTimer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(DeviceConnectedVC.refreshRSSI), userInfo: nil, repeats: true)
        RunLoop.current.add(rssiReloadTimer!, forMode: RunLoop.Mode.common)
    }
    
    @objc func refreshRSSI(){
        print("refreshRSSI")
        CT007DisplayOnScreen()
        //notify the variables to other camera view controllers
        NotificationCenter.default.post(name: Notification.Name(rawValue: myCT007NotificationKey), object: self)
        Radiation_Characteristics_Value_Int = 0
    }
    
    @objc func doThisWhenNotifyCT007() {
        print("notification...001")
    }
    
    func CT007DisplayOnScreen (){
        OneSecTimer = OneSecTimer + 1
        periphGlob = self.peripheralVar
        if self.peripheralVar?.state == CBPeripheralState.connected {
            Connected = true
            ConnectionState = "Connected"
        }else if self.peripheralVar?.state == CBPeripheralState.disconnected {
            Connected = false
            ConnectionState = "Disconnected"
        }else if self.peripheralVar?.state == CBPeripheralState.connecting {
            Connected = false
            ConnectionState = "Scanning"
        }else if self.peripheralVar?.state == CBPeripheralState.disconnecting {
            Connected = false
            ConnectionState = "Disconnecting"
        }
        
        if OneSecTimer == 2 , Connected {
            consoleTimer = Timer.scheduledTimer(timeInterval: 0.5,
                                                    target: self,
                                                    selector: #selector(DeviceConnectedVC.GetConsoleData),
                                                    userInfo: nil,
                                                    repeats: true)
        }
        
        print("Radiation_Characteristics_Value:   " , Radiation_Characteristics_Value_Int)
//<<<<<<< HEAD
        //Time correction
        print("Counts: \(Radiation_Characteristics_Value_Int)")
        
        if timeToReconnect >= 0{
            disconnectAlertController?.message = "The detector was disconnected. GammaGuard will attempt to reconnect every \(reconnectionTime) seconds" + "\nTime to reconnect: " + String(timeToReconnect)
            //            alertController?.textFields?.last?.text = "Time to reconnect: " + String(timeToReconnect)
            timeToReconnect -= 1
        }
//=======
//>>>>>>> master
    }
    
    func SendConsole (msg: String){
        SentMsg = msg
        print ("sent to console : ", msg)
        let command = msg + "\n"
        if let command = command.data(using: String.Encoding.ascii, allowLossyConversion: false) {
            write(command, characteristic: MyOutPutChar!, type: .withResponse)
        }
    }
    
    func write(_ data: Data, characteristic: CBCharacteristic, type: CBCharacteristicWriteType) {
        print ("..........Device.writing....014..... (001): ", characteristic)
        peripheralVar?.writeValue(data, for: characteristic, type: type)
    }
    
    @objc func readAfterConnect(){
        if consoleTimer == nil{
            consoleTimer = Timer.scheduledTimer(timeInterval: 0.5,
                                                target: self,
                                                selector: #selector(DeviceConnectedVC.GetConsoleData),
                                                userInfo: nil,
                                                repeats: true)
        }
    }
    
    //This tells detector to stop, send info and restart timer
    @objc func GetConsoleData(){
        MillisecondsTimer = MillisecondsTimer + 1
        if !Connected{MillisecondsTimer = 0}
        print ("getting timer 300 milis ", MillisecondsTimer )
        connectingAlertController?.message = "The connection was found, reconnecting to device...\n(\(MillisecondsTimer)/15)"
        if MillisecondsTimer == 1 , Connected {
            //Check if device is ready
            SendConsole(msg: command_RESPOND_TEST)
            print ("checking if device is ready")
        }
        if MillisecondsTimer == 2 , Connected {
            SendConsole(msg: command_STOP_TIMER0)
            SendConsole(msg: command_STOP_TIMER2)
            print ("getting stopping timers")
        }
        if MillisecondsTimer == 3 , Connected {
            SendConsole(msg: command_PRINT_V)
            print ("getting Conversion Factor \(GOT_CONVERSION)")
        }
        if MillisecondsTimer == 4 , Connected {
            SendConsole(msg: command_PRINT_VERSION)
            print ("Getting Basic Version \(GOT_BASIC_VERSION)")
        }
        if MillisecondsTimer == 5 , Connected {
            SendConsole(msg: command_PRINT_DEVICE_ID)
            print ("getting Detector ID  \(GOT_DEVICE_ID)")
        }
        if !CT_Device {
            if MillisecondsTimer == 6 , Connected {
                SendConsole(msg: command_LIST)
                print ("getting Detector Name  \(GOT_DEVICE_NAME)")
                Detector_NAME = GOT_DEVICE_NAME
            }
        }else{
//            DetectorNameLabel.text = "This is a CT-detector : " +  Detector_NAME!
        }

        if MillisecondsTimer == 7 , Connected {
            SendConsole(msg: command_PRINT_Beep)
            print ("getting beeping state  \(GOT_DEVICE_ID)")
        }
        if MillisecondsTimer == 8 , Connected {
            SendConsole(msg: command_PRINT_RTime)
            print ("getting Response Time  \(GOT_DEVICE_ID)")
        }
        if MillisecondsTimer == 9 , Connected {
            SendConsole(msg: command_PRINT_DTime)
            print ("getting Dead Time  \(GOT_DEVICE_ID)")
        }
        if MillisecondsTimer == 10 , Connected {
            SendConsole(msg: command_PRINT_Alarm)
            print ("getting alarm state  \(GOT_DEVICE_ID)")
        }
        if MillisecondsTimer == 11 , Connected {
            SendConsole(msg: command_PRINT_SCREENS)
            print ("getting screens state  \(GOT_DEVICE_ID)")
        }
        if MillisecondsTimer == 12 , Connected {
            SendConsole(msg: command_PRINT_UNITS)
            print ("getting units state  \(GOT_DEVICE_ID)")
        }
        if MillisecondsTimer == 13 , Connected {
            SendConsole(msg: command_PRINT_Alarm_Level)
            print ("getting units state  \(GOT_DEVICE_ID)")
        }
        if MillisecondsTimer == 14 , Connected {
            SendConsole(msg: command_CLEAR_COUNTER)
            SendConsole(msg: command_BEFORE_TIMERS)
            SendConsole(msg: command_RUN_TIMER_0)
            SendConsole(msg: command_RUN_TIMER_2)
            print ("getting running timers again")
        }
        if MillisecondsTimer == 15 , Connected {
            if !presentedMainView{
                performSegue(withIdentifier: "connectingToConnected", sender: self)
            }
            presentedMainView = true
            if consoleTimer != nil {
                consoleTimer?.invalidate()
                consoleTimer = nil
            }
            MillisecondsTimer = 0
            removeConnectingAlert()
            print("getting timer 300 mi \(MillisecondsTimer)")
        }
    }
}
