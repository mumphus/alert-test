//
//  CT007.swift
//  CT007
//
//  Created by Sepehr Khatir on 15/03/2017.
//  Copyright © 2017 EIC. All rights reserved.
//

import Foundation
import CoreBluetooth

/********** Values **********/
// device name to be identified
//let deviceName = "CT007-B"
//let deviceName = "CT-S-09"
//let deviceName = "CT-P-09"
//let deviceName = "CT007-22"
//let deviceName = "CT007-EB"
//var deviceName = "detector"
/********** basic names **********/
// device basic name to be identified
//let alternativeName = "BASIC#E4"
//let alternativeName = "BASIC#09"
//let alternativeName = "BASIC#22"
//let alternativeName = "BASIC#EB"

// service and characteristic received while display isn't working
// most likely related to background mode - no useful data but does demonstrate device communication

//let deviceUUID = CBUUID(string: "25fb9e91-1616-448d-b5a3-f70a64bda73a")
//let deviceCharacteristic = CBUUID(string: "d6af9b3c-fe92-1cb2-f74b-7afb7de57e6d")

let commsServiceUUID = CBUUID(string: "25FB9E91-1616-448D-B5A3-F70A64BDA73A")
let inputCharacteristicUUID = CBUUID(string: "C3FBC9E2-676B-9FB5-3749-2F471DCF07B2")
let outputCharacteristicUUID = CBUUID(string: "D6AF9B3C-FE92-1CB2-F74B-7AFB7DE57E6D")

let oadServiceUUID = CBUUID(string: "F000FFC0-0451-4000-B000-000000000000")
let imgIdentityUUID = CBUUID(string: "F000FFC1-0451-4000-B000-000000000000")
let imgBlockUUID = CBUUID(string: "F000FFC2-0451-4000-B000-000000000000")

let deviceInfoServiceUUID = CBUUID(string: "180A")
let firmwareRevisionUUID = CBUUID(string: "2A26")

// Radiation Count Measurement - should be unique to device
let radiationService = CBUUID(string: "f100ffd0-0451-4100-b100-000000000000")
let radiationCount = CBUUID(string: "f100ffd1-0451-4100-b100-000000000000")
let Tot_Rad_Count = CBUUID(string: "f100ffd2-0451-4100-b100-000000000000")
let Conversion_Factor = CBUUID(string: "f100ffd3-0451-4100-b100-000000000000")
let Enable_Buzzer = CBUUID(string: "f100ffd5-0451-4100-b100-000000000000")
let Rad_Dose_Rate_Alert = CBUUID(string: "f100ffd6-0451-4100-b100-000000000000")

let batteryService = CBUUID(string: "0000180f-0000-1000-8000-00805f9b34fb")
let batteryLevel = CBUUID(string: "00002a19-0000-1000-8000-00805f9b34fb")





// Placeholders for radiation count array
var radCounts = [Int]()
var i = -1
var sum = 0
var avg = 0
/***************************/

class CT007 {
    
    // Check name of device from advertisement data
//    class func deviceFound (_ advertisementData: [AnyHashable: Any]!) -> Bool {
//        let nameOfDeviceFound = (advertisementData as NSDictionary).object(forKey: CBAdvertisementDataLocalNameKey) as? String
//        print ("name of my device is : " , nameOfDeviceFound)
//
//        if (nameOfDeviceFound?.range(of: "CT-") != nil) {
//          deviceName = nameOfDeviceFound!
//            return true
//        }else{
//            deviceName = "not a CT"
//            return false
//        }
//        
//        }
    
    // Check if the service has a valid UUID
    
    
    class func isCommService (_ service : CBService) -> Bool {
        if String(describing: service.uuid).count != 0 {
            return (service.uuid == commsServiceUUID)
        }
        return false
    }
    
    class func isoadService (_ service : CBService) -> Bool {
        if String(describing: service.uuid).count != 0 {
            return (service.uuid == oadServiceUUID)
        }
        return false
    }
    
    class func isBatteryService (_ service : CBService) -> Bool {
        if String(describing: service.uuid).count != 0 {
            return (service.uuid == batteryService)
        }
        return false
    }
    
    class func isRadiationService (_ service : CBService) -> Bool {
        if String(describing: service.uuid).count != 0 {
            return (service.uuid == radiationService)
        }
        return false
    }
    
    // Check if the characteristic has a valid UUID
    
    class func isInputCharacteristic (_ characteristic : CBCharacteristic) -> Bool {
        if String(describing: characteristic.uuid).count != 0 {
            return (characteristic.uuid == inputCharacteristicUUID)
        }
        return false
    }

    class func isOutputCharacteristic (_ characteristic : CBCharacteristic) -> Bool {
        if String(describing: characteristic.uuid).count != 0 {
            return (characteristic.uuid == outputCharacteristicUUID)
        }
        return false
    }
    
    
    class func isImgIdentityCharacteristic (_ characteristic : CBCharacteristic) -> Bool {
        if String(describing: characteristic.uuid).count != 0 {
            return (characteristic.uuid == imgIdentityUUID)
        }
        return false
    }
    
    class func isimgBlockCharacteristic (_ characteristic : CBCharacteristic) -> Bool {
        if String(describing: characteristic.uuid).count != 0 {
            return (characteristic.uuid == imgBlockUUID)
        }
        return false
    }

    
    class func isBatteryCharacteristic (_ characteristic : CBCharacteristic) -> Bool {
        if String(describing: characteristic.uuid).count != 0 {
            return (characteristic.uuid == batteryLevel)
        }
        return false
    }
    
    class func isRadiationCharacteristic (_ characteristic : CBCharacteristic) -> Bool {
        if String(describing: characteristic.uuid).count != 0 {
            return (characteristic.uuid == radiationCount)
        }
        return false
    }
    
    class func isConversionCharacteristic (_ characteristic : CBCharacteristic) -> Bool {
        if String(describing: characteristic.uuid).count != 0 {
            return (characteristic.uuid == Conversion_Factor)
        }
        return false
    }
    

}

