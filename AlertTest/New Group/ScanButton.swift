//
//  ScanButton.swift
//  BLEScanner
//
//  Created by Sepehr Khatir on 15/03/2017.
//  Copyright © 2017 EIC. All rights reserved.
//

import UIKit

class ScanButton: UIButton {
    
    required init?(coder aDecoder: NSCoder){
        super.init(coder: aDecoder)
        
        layer.borderWidth = 1.5
        layer.borderColor = UIColor.blue.cgColor
    }
    
    func buttonColorScheme(_ isScanning: Bool){
        let title = isScanning ? "Stop Scanning" : "Start Scanning"
        setTitle(title, for: UIControl.State())
        
        let titleColor = isScanning ? UIColor.blue : UIColor.white
        setTitleColor(titleColor, for: UIControl.State())
        
        backgroundColor = isScanning ? UIColor.clear : UIColor.blue
    }
}
